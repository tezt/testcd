rm -rf .git .new
git clone https://codeberg.org/zz/sh .new
mv .new/.git .
cp ../i.sh .

git add *

GIT_AUTHOR_DATE="2022-06-29T18:00:00 +0200" GIT_COMMITTER_DATE=$GIT_AUTHOR_DATE git commit --date="10 day ago" -am "
continuation of https://gitlab.com/z.z/sh

Functions :
Major changes

GO -> MAIN
PRINT - print with empty lines above and below
SUB - substitute string
CCFG - check if grub.cfg exist

Distros :
added: VOID-musl GUIX
removed : KISS

Kernel parameter: rootfstype 

extra files including post.sh should be stored inside asset/

All mount operations under prepare

Variable: CEFI - to check if system is efi

update grub config : -o not >>

lsblk column: mountpoints removed (as not available on antix)

default: compression for fs, grub version for nixos

awk -> gawk

"


