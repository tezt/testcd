#!/bin/sh

# SPDX-FileCopyrightText: 2022 XADE <xad3play@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

## FUNCTIONS

# print
PRINT	() {
	printf '\n\n\n%s\n\n\n' "$1"
	}
# print the script name and all arguments to stderr
YELL	() { 
	printf '\033[7m\n%s\n\033[m' "($0) FAILED : $*" >&2
	}
# YELL and exit
DIE	() { 
	YELL "$*"
	exit 1
	}
# DIE if command fails
TRY	() { 
	eval "$1" || 
	DIE "$*"
	}
# warn if command fails
WARN	() {
	eval "$1" || 
	YELL "$*"
	}
# execute command inside chroot environment
EXE	() {
	PRINT '' | chroot "${DIR}" sh -c "$1"
	}
# show block device info
BLK	() {
	lsblk -lspo name,type,fstype,uuid,mountpoints "$@"
	}
# add new user
USERADD	() {
	PRINT "$1:x:501:501::/home/$1:${SHELL}" >> /etc/passwd
	PRINT "$1::::::::" >> /etc/shadow
	PRINT "$1:x:501:$1" >> /etc/group
	}
# substitute string of a file in rootfs
SUB     () {             
	FILE="${TMP}${1##*/}"
	awk '{gsub("'"$2"'", "'"$3"'", $0);}1' "${DIR}$1" >| "${FILE}"
	cp "${FILE}" "${DIR}$1"
	# usage: SUB <path/to/file> <string> <new_string>
	}
# filter out only the matched REGEX according to arguements
FLT	() {
	curl -L "$2" | awk -v REGEX="$3" -v AFTER="$4" '$0 ~ AFTER {f=1}f {match($0,REGEX,m); printf substr(m[0],2,length(m[0])-2)" "}' | awk -v FIELD="$1" '{printf $FIELD}' | awk '{printf $NF}'
	}
# get the file from provided url
GET	() {	
	curl -L "$1" >| "${FETCHED}"
	}
# filter, get and extract file according to arguements
FGE	() {
	# get the first file that matches REGEX
	FILE=$(FLT "$@") 
	TRY "
	GET $6${FILE} || 
	GET $2/${FILE}
	"
	# extract the archive  
	TAR="tar -xf ${FETCHED} -C ${DIR}$5"
	TRY "
	eval ${TAR} --xz   ||
	eval ${TAR} --gzip ||
	eval ${TAR} --zstd 
	"
	# usage : FGE <index of match, "" for last match> <URL> <REGEX to look for> <REGEX to look ahead of> <directory to extract in> <switch domain with>
	}
# set the mount point and mount, warn about failure
MNT	() {
	# arguements passed
	ARG="$*"
	# last arguement
	LAST="$(printf '%s\n' "${ARG}" | awk '{printf $NF}')"
	# arguements before last
	REST=${ARG%%"$LAST"}
	# set the mount point
	MPT="${DIR}${LAST}"
	mkdir -p "${MPT}"
	WARN "mount ${REST} ${MPT}"
	}
# prepare devices
PREPARE	() {
	# create a directory for carrying out the installation
	mkdir -p "${DIR}"
	# mount the root device
	MNT "${ROOT}" /
	# create a subvolume for rootfs
	WARN "btrfs subvolume create ${DIR}${OS}"
	# unmount root device
	umount -Rl "${DIR}"
	# mount the subvolume on ${DIR}
	MNT -o noatime,compress=zstd,space_cache=v2,subvol="${OS}" "${ROOT}" /
	# mount root device again if it is not a btrfs device
	MNT "${ROOT}" /
	# mount necessary dir 
	MNT "${FAT}" fat
	MNT -R /sys  sys 
	MNT -R /dev  dev 
	MNT -R /run  run 
	MNT -R /proc proc
	# create needed directories inside rootfs
	mkdir -p "${DIR}boot/grub" "${DIR}usr/src" "${TMP}" "${DIR}etc/default" "${DIR}etc/mkinitfs" "${DIR}etc/dracut.conf.d" "${DIR}run/shm" "${FMP}grub" /nix /gnu
	# copy content of current directory to rootfs/tmp, to access them from inside of chroot environment
	cp -rp ./. "${TMP}"
	}
# chroot and exec scripts
CHROOT	() {
	# specify modules to be included inside initramfs, by dracut
	PRINT "dracutmodules+=\" rootfs-block kernel-modules base crypt lvm resume \"" >| "${DIR}etc/dracut.conf.d/easy.conf"
	# specify HOOKS to be included inside initramfs, by mkinitcpio
	PRINT "HOOKS=(base udev autodetect modconf block filesystems keyboard fsck keymap lvm2 encrypt resume)" >| "${DIR}etc/mkinitcpio.conf"
	# specify features to be included inside initramfs, by mkinitfs
	PRINT "features=\"base btrfs lvm raid cryptsetup resume nvme usb ata ext4 scsi\"" >| "${DIR}etc/mkinitfs/mkinitfs.conf"
	# generate initramfs
	dracut -N -f -m " rootfs-block kernel-modules base crypt lvm " "${DIR}boot/cust.img" --no-kernel
	# fstab: set of rules to mount devices
	PRINT "
# device	mount-point	fs-type	options			dump pass
UUID=${FUID}	/fat 		vfat	rw,noatime,defaults	0 2
	" >| "${DIR}etc/fstab"
	# copy the DNS config inside rootfs for network access
	cp /etc/resolv.conf "${DIR}etc/"
	# copy portage config inside rootfs
	cp "${TMP}make.conf" "${DIR}etc/portage/"
	# disable space and sign checking by pacman
	SUB /etc/pacman.conf "^.*CheckSpace" "#CheckSpace"
	SUB /etc/pacman.conf "Required DatabaseOptional" "Never"
	# configure grub [bootloader]
	PRINT "
GRUB_DEFAULT=0
GRUB_TIMEOUT=0
GRUB_GFXMODE=auto
GRUB_TERMINAL=console
GRUB_TIMEOUT_STYLE=menu
GRUB_DISTRIBUTOR=\"${OS}\"
GRUB_ENABLE_CRYPTODISK=y
GRUB_GFXPAYLOAD_LINUX=keep
GRUB_DISABLE_RECOVERY=true
GRUB_DISABLE_OS_PROBER=false
GRUB_EARLY_INITRD_LINUX_CUSTOM=\"cust.img\"
GRUB_PRELOAD_MODULES=\"lvm luks luks2 part_gpt cryptodisk gcry_rijndael pbkdf2 gcry_sha256 btrfs\"
GRUB_CMDLINE_LINUX_DEFAULT=\"root=UUID=${RUID} rd.luks.uuid=${LUID} cryptdevice=UUID=${LUID}:${OS} cryptroot=UUID=${LUID} cryptdm=${OS} rootfstype=${FS}\"
	" >| "${DIR}etc/default/grub"
	# steps to install grub
	PRINT "
cd /fat/ || exit 1
dracut -N -f -m ' rootfs-block kernel-modules base crypt lvm ' /boot/cust.img --no-kernel
# install for legacy
${GIN}=i386-pc
# install for efi, on removable device
${GIN}=x86_64-efi --removable
grub-mkconfig >| /boot/grub/grub.cfg
	" >> "${CHT}"
	# execute chroot script
	EXE "sh /tmp/CHT"
	# execute post script
	EXE "sh /tmp/post.sh"
	# enable signature checking by pacman
	SUB /etc/pacman.conf "Never" "Required DatabaseOptional"
	# empty root passwd
	SUB /etc/shadow "root:.*$" "root::::::::"
	# link init if it's not already
	ln -s "$(printf '%s\n' "${DIR}usr/bin/"*-init | awk -F '/' '{printf $NF; exit}')" "${DIR}usr/bin/init"
	}
# installation begins here
MAIN	() {

	## PROMPTS
	
	# name this installation
	printf "
\033[2J\033[H 
 > NAME      : "
	read -r OS
	# list supported DISTRO IDs
	printf "
[ DISTRO ID ] | [ DISTRO ID ]
--------------+--------------
  ARCH        |   ALPINE
  ARTIX       |   GENTOO
  FUNTOO      |   KISS
  GUIX        |   NIXOS 
  VOID        |   VOID -musl

 > DISTRO ID : "
	# choose a distro by DISTRO ID
	read -r DID
	# list the packages to install
	printf "
 > packages  : "
	read -r PKG
	# list supported fat devices
	BLK | awk '/vfat/ {printf "\n\t"$1"\n"}'
	printf "
 > FAT  device : "
	# select fat device for grub
	read -r FAT
	# list supported root devices
	BLK | awk '/btrfs/ {printf "\n\t"$1"\n"}'
	printf "
 > ROOT device : "
	# select device for rootfs
	read -r ROOT
	
	## VARIABLES
	
	# new rootfs directory for installation
	DIR="/tmp/${OS}/"
	# fat mount point
	FMP="${DIR}fat/"
	# temporary directory inside rootfs
	TMP="${DIR}tmp/"
	# path to save fetched files
	FETCHED="${TMP}FETCHED"
	# chroot script
	CHT="${TMP}CHT"
	# memory
	MEM="0$(awk '/MemAvailable/ {printf $2}' /proc/meminfo)"
	# swap
	SWP="0$(awk 'NR==2 {printf $3}' /proc/swaps)"
	# max possible jobs = {(swap + memory)/3GB}+1GB
	MPJ=$(awk -v MEM="${MEM}" -v SWP="${SWP}" 'BEGIN{printf substr(((MEM+SWP)/3000000)+1,1,1)}')
	# cpu threads
	CPU=$(nproc)
	# feasible number of jobs according to system resources
	JOB=$(awk -v A="${MPJ}" -v B="${CPU}" 'BEGIN{printf (A>=B)*B+(B>A)*A}')
	# detect disk for boot loader installation
	DISK=$(BLK "${FAT}" | awk '/disk/ {printf $1; exit}')
	# LUKS uuid
	LUID=$(BLK "${ROOT}" | awk '/LUKS/ {printf $4; exit}')
	# root uuid
	RUID=$(BLK "${ROOT}" | awk 'NR==2 {printf $4}')
	# rootfs type
	FS=$(BLK "${ROOT}" | awk 'NR==2 {printf $3}')
	# fat uuid
	FUID=$(BLK "${FAT}" | awk 'NR==2 {printf $4}')
	# check efi
	CEFI=$(cd /sys/firmware/efi && printf "y")
	# command to install grub [bootloader]
	GIN="grub-install --boot-directory=. --efi-directory=. ${DISK} --target"
	# mirror to fetch files from
	MIR="https://mirrors.tuna.tsinghua.edu.cn"
	# detect architecture
	ARC=$(uname -m)
	# compression algorithm
	COMP=".*(gz|xz|zst)(\"|<)"
	# steps to compile and install kernel
	KIN="
cd /usr/src/linux-*/ || exit 1
cp /tmp/.config ./ || 
make localyesconfig
printf '%s' '
CONFIG_DEBUG_INFO=n
' >> .config
make oldconfig
make
make INSTALL_MOD_STRIP=1 modules_install
make install
"
	# export variable to access them from inside of chroot environment
	export PATH="${PATH}:/bin:/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/run/wrappers/bin:/root/.nix-profile/bin:/etc/profiles/per-user/root/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin" LOGNAME="${OS}" MAKEOPTS="-j${JOB}" MAKEFLAGS="-j${JOB}"
	PREPARE
	# prepare rootfs of specified distro
	TRY "${DID}"
	CHROOT
	# display and add generated grub.cfg to grub menu
	awk '1' "${DIR}boot/grub/grub.cfg" && PRINT "
menuentry ${OS} {
cryptomount -u $(printf '%s' "${LUID}" | awk '{gsub("-","")}1')
search --set=root --fs-uuid ${RUID}
configfile /${OS}/boot/grub/grub.cfg}
	" >> "${FMP}grub/grub.cfg"
	PRINT "can REBOOT now"
	}

## ADD A NEW DISTRO 

### step 1: define a function as DISTRO_ID () {}
#DISTRO	() {
### step 2: get the rootfs of new distro
###	option 1: use a script to create rootfs from scratch
###	option 2: fetch archive from an url [example url=mirror.domain/downloads/disrto-ver.tar.xz]
#	FGE 1 "mirror/downloads" ">distro${COMP}" 
###	option 3: extract rootfs from distro.iso [stupid option: wastage of bandwidth]
#	FGE 1 "mirror/downloads" ">distro.*iso<" 
###		fetched files are saved as ${FETCHED}
#	MNT -o loop "${FETCHED}" tmp/iso
###		for example, destination of rootfs image is ${TMP}iso/LiveOS/rootfs.img
#	MNT -o loop "${TMP}iso/LiveOS/rootfs.img" tmp/iso
###		copy contents of rootfs to ${DIR}
#	cp -r "${TMP}iso/." "${DIR}"
### step 3: create chroot script with steps and commands to setup the distro
#      	PRINT "
#<package_manager> <install> ${PKG}
#	" >| "${CHT}"
#      	 	}
### a separate section is created inside of function: CHROOT () {} to install grub, modify that instead of including it inside ${CHT}
### step 4: check and modify other segments of this script if needed

ARCH	() {
	GET codeberg.org/zz/strap/raw/branch/master/pacstrap.sh
	sh "${FETCHED}" "${DIR}"
	EXE "
update-ca-trust
pacman -Syu --overwrite '*' pacman
	"
	FGE 1 "${MIR}/archlinux/core/os/x86_64" ">pacman-[^a-z]${COMP}"
	curl -L archlinux.org/mirrorlist/all | awk '{printf substr($0,2)"\n"}' >| "${DIR}etc/pacman.d/mirrorlist"
	PRINT "
pacman -Syu --overwrite '*' ${PKG}
pacman-key --init
pacman-key --populate archlinux
	" >| "${CHT}"
	}
ARTIX	() {
	GET codeberg.org/zz/strap/raw/branch/master/pacstrap.sh
	sh "${FETCHED}" "${DIR}"
	PRINT "
update-ca-trust
pacman -Syu --overwrite '*' ${PKG}
pacman-key --init
pacman-key --populate artix
	" >| "${CHT}"
	}
ALPINE	() {
	FGE 1 "${MIR}/alpine/latest-stable/releases/${ARC}" ">alpine-minirootfs.*[^a-z].-${ARC}${COMP}" 
	PRINT "
apk add ${PKG}
	" >| "${CHT}"
	}
VOID	() {
	FGE 1 "${MIR}/voidlinux/live/current" ">void-${ARC}$1-ROOTFS${COMP}"
	PRINT "
rm -rf /var /usr/share/zoneinfo
xbps-install -R ${MIR}/voidlinux/current/ -S ${PKG}
	" >| "${CHT}"
	}
GENTOO	() {
	FGE 1 "${MIR}/gentoo/releases/amd64/autobuilds/current-install-amd64-minimal" ">stage3-amd64-hardened-openrc${COMP}"
	PRINT "
mkdir -p /etc/portage/repos.conf
cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
emerge-webrsync
env-update
. /etc/profile
USE=\"device-mapper\" emerge -uqDN --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going ${PKG}
${KIN}
	" >| "${CHT}"
	}
FUNTOO	() {
	FGE 1 "build.funtoo.org/next/x86-64bit/generic_64" ">stage3${COMP}"
	PRINT "
ego sync
USE=\"device-mapper\" emerge -uqDN --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going ${PKG}
${KIN}
	" >| "${CHT}"
	}
KISS	() {
	FGE 1 "github.com/kisslinux/repo/releases" "/download${COMP}"
	FGE 1 "kernel.org" "/cdn.kernel.org/pub/linux${COMP}" "longterm" "usr/src/"
	SUB /usr/src/linux-*/tools/objtool/arch/x86/decode.c "stdlib.h" "stdlib.h\n#include <linux/stddef.h>\n"
	# kiss config
	PRINT "
export CFLAGS=\"-O3 -pipe -march=native\"
export CXXFLAGS=\"\$CFLAGS\"
export MAKEFLAGS=\"-j${JOB}\"
export REPOS_DIR='/var/db/kiss'
export KISS_PATH=''
export KISS_SU=su
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/core
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/extra
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/wayland
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/community/community
	" >> "${DIR}etc/profile.d/kiss_path.sh"
	PRINT "
. /etc/profile.d/kiss_path.sh
cd \$REPOS_DIR/ || exit 1
git clone https://github.com/kisslinux/repo
git clone https://github.com/kiss-community/community
kiss b gnupg1
kiss i gnupg1
gpg --keyserver keys.gnupg.net --recv-key 13295DAC2CF13B5C
printf '\ntrusted-key 0x13295DAC2CF13B5C\n' >> /root/.gnupg/gpg.conf
cd  \$REPOS_DIR/repo/ || exit 1
git config merge.verifySignatures true
kiss update
kiss update
kiss b ${PKG}
kiss i ${PKG}
${KIN}
	" >| "${CHT}"
	}
NIXOS	() {
	USERADD "nixbld"
	GET nixos.org/nix/install
	sh "${FETCHED}"
	. "${HOME}/.nix-profile/etc/profile.d/nix.sh"
	nix-channel --add "${MIR}/nix-channels/nixos-unstable" nixpkgs
	nix-channel --update
	nix-env -f '<nixpkgs>' -iA nixos-install-tools
	nixos-generate-config --root "${DIR}"
	PRINT "
{ config, pkgs, ... }:
{
 imports = [ ./hardware-configuration.nix ];
 boot = {
	loader.grub = {
		version = 2;
		enable = true;
		device = \"${DISK}\";
		efiSupport = true;
		useOSProber = true;
		enableCryptodisk = true;
		efiInstallAsRemovable = true;
		extraEntries = ''
			menuentry OTHER {
			search --set=root --fs-uuid ${FUID}
			configfile /grub/grub.cfg}'';
	};
	loader.efi = {
		efiSysMountPoint = \"/fat\";
	};
 };
 networking.nameservers = [ \"9.9.9.9\" ];
 environment.systemPackages = with pkgs; [ ${PKG} ];
 system.stateVersion = \"21.11\"; # Do not modify, before going through the manual.
}
	" >| "${DIR}etc/nixos/configuration.nix"
	TRY "PRINT '' | nixos-install --root ${DIR}"
	}
GUIX	() {
	USERADD "guixbuild"
	FGE "" "https://ftp.gnu.org/gnu/guix/" ">guix-binary.*${ARC}${COMP}"
	cp -rp "${DIR}gnu" "${DIR}var" /
	GP="/var/guix/profiles/per-user/root/current-guix"
	. "${GP}/etc/profile"
	guix-daemon --build-users-group=guixbuild &
        guix archive --authorize < "${GP}/share/guix/ci.guix.gnu.org.pub"
        guix archive --authorize < "${GP}/share/guix/bordeaux.guix.gnu.org.pub"
	PRINT "
(use-modules (gnu))
(use-service-modules networking)
(define (append-to-computed-file g text)
  #~(begin
      #\$g
      (let ((port (open-file #\$output \"a\")))
        (format port #\$text)
        (close port))))
(define %grub-other_entries \"
menuentry OTHER {
search --set=root --fs-uuid ${FUID}
configfile /grub/grub.cfg}
\")
(define* (grub-conf-with-custom-part fn)
  (lambda* (#:rest r)
    (let ((grubcfg-computed-file (apply fn r)))
      (computed-file
       (computed-file-name grubcfg-computed-file)
       (append-to-computed-file
        (computed-file-gexp grubcfg-computed-file)
        (string-append
         %grub-other_entries))
       #:options (computed-file-options grubcfg-computed-file)))))
(operating-system
  (host-name \"${OS}\")
  (bootloader (bootloader-configuration
    (bootloader (bootloader 
      (inherit grub-${CEFI:+efi-removable-}bootloader)
      (configuration-file-generator
        (grub-conf-with-custom-part
	  (bootloader-configuration-file-generator grub-${CEFI:+efi-removable-}bootloader)))))
    (targets (list ${CEFI:+\"/fat\"} \"${DISK}\"))))
  (mapped-devices (list 
    (mapped-device
      (source (uuid \"${LUID:-\"\"}\"))
      (target \"${OS}\")
      (type luks-device-mapping))))
  (file-systems 
    (append (list
      (file-system
        (device (uuid \"${RUID}\"))
        (mount-point \"/\")
        (type \"btrfs\")
        (options \"subvol=${OS}\")
        ${LUID:+(dependencies mapped-devices)})
      (file-system
        (device (uuid \"${FUID}\" 'fat))
        (mount-point \"/fat\")
        (type \"vfat\"))) 
	%base-file-systems))
  (services
    (append (list 
      (service dhcp-client-service-type))
      %base-services)))
	" >| "${DIR}etc/config.scm"
	TRY "PRINT '' | guix system init ${DIR}etc/config.scm ${DIR}"
	}

## CHECK REQUIREMENTS and START INSTALLATION

TRY "chroot / sh -c \"type awk curl btrfs tar xz gzip zstd dmsetup mount lsblk || exit 1\""
MAIN
