#!/bin/sh

# artix-bootstrap: Bootstrap a base Artix Linux system using any GNU distribution.
#
# requirements: sh curl gawk tar gzip xz zstd 
#
# Usage:
#
#   # sh artix_bootstrap.sh
#   # sh artix_bootstrap.sh -i openrc -r https://mirrors.tuna.tsinghua.edu.cn/artixlinux/system/os/x86_64 -d destination
#
# And then you can chroot to the destination directory
#
#   # chroot destination

# get the script to bootstrap pacman, if curl feels unsafe place the script manually
curl -OL codeberg.org/zz/strap/raw/branch/master/pacstrap.sh

# separate arguements
ARG="$*"
SET_ARG () {
	printf '%s' "$(printf '%s\n' "${ARG}" | awk -F "$1 " '{printf $2}' | awk '{printf $1}')"
}

INIT=$(SET_ARG -i)
DEST=$(SET_ARG -d)
REPO=$(SET_ARG -r)

# default options
INIT="${INIT:-dinit}"
DEST="${DEST:-./artix_${INIT}}"
EXEC="artix_${INIT}.sh"
REPO="${REPO:-https://mirrors.tuna.tsinghua.edu.cn/artixlinux/system/os/x86_64}"

# bootstrap pacman
awk "/URL/{printf URL=${REPO}}1" pacstrap.sh >| "${EXEC}"
awk -v REPO="${REPO}" '/URL/{printf "URL="REPO"\n"}1' pacstrap.sh >| "${EXEC}"
sh "${EXEC}" "${DEST}"

# install base artix with preferred init
export INIT="${INIT}" PATH="$PATH:/bin:/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin"
chroot "${DEST}" sh -c "
update-ca-trust
pacman -Syu --overwrite '*' base linux seatd-${INIT}
pacman-key --init
pacman-key --populate artix
pacman --version
"

# enable signature check
cp "${DEST}/etc/pacman.conf" ./pacman.conf
awk '{gsub ("^[[:space:]]*SigLevel.*$", "SigLevel = Required DatabaseOptional", $0);}1' ./pacman.conf >| "${DEST}/etc/pacman.conf" 

