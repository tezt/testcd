#!/bin/sh

# SPDX-FileCopyrightText: 2022 XADE <xad3play@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

## FUNCTIONS

# block device info
BLK	() {
	lsblk -lspo name,type,fstype,uuid $1
	}
# filter
FLT	() {
	printf '%s' "$(curl -L "$2" | awk -v REGEX="$3.*(gz|xz|zst)(\"|<)" -v AFTER="$4" '$0 ~ AFTER {f=1}f {match($0,REGEX,m); printf substr(m[0],2,length(m[0])-2)" "}' | awk -v FIELD="$1" '{printf $FIELD}')" 
	}
# get & extract
GET	() {
	curl -L "$1" -o "$DF"
	TAR="tar -xf $DF -C $DIR/$2"
	eval "$TAR" --xz ||
	eval "$TAR" --gzip ||
	eval "$TAR" --zstd
	}
# filter & get & extract
FET	() {
	FILTER=$(FLT 1 "$@") 
	GET "$FILTER" "$4" || 
	GET "$1/$FILTER" "$4" 
	# syntax : FET <URL> <REGEX to look for> <REGEX to look ahead of> <directory to save in>
	}
# prepare volume
PRE	() {
	mkdir -p "$DIR"
	mount "$ROOT" "$DIR"
	btrfs subvolume create "$DIR/$OS"
	mount -R "$DIR/$OS" "$DIR"
	mkdir -p "$FMP" "$DIR/usr/src" "$TMP" "$DIR/etc/mkinitfs" "$DIR/etc/dracut.conf.d" "$DIR/run/shm"
	mount "$FAT" "$FMP"
	# copy content of current directory to rootfs/tmp for later use
	cp -r ./. "$TMP"
	}
CHROOT	() {
	# null passwd
	printf 'root::18678:0:99999:7:::' >| "$DIR/etc/shadow"
	# initramfs modules
	printf 'dracutmodules+=" rootfs-block kernel-modules base crypt lvm resume "' >| "$DIR/etc/dracut.conf.d/easy.conf"
	printf 'HOOKS=(base udev autodetect modconf block filesystems keyboard fsck keymap lvm2 encrypt resume)' >| "$DIR/etc/mkinitcpio.conf"
	printf 'features="ata base ide scsi usb virtio btrfs lvm raid kms cryptsetup nvme resume"' >| "$DIR/etc/mkinitfs/mkinitfs.conf"
	# fstab
	printf '%s' "
UUID=$RUID  /  btrfs  rw,noatime,compress=zstd:3,space_cache=v2,subvol=/$OS  0 0
UUID=$FUID  /fat  vfat  rw,noatime,defaults  0 2
	" >| "$DIR/etc/fstab"
	# bind dirs
	mount -R /sys  "$DIR/sys"
	mount -R /dev  "$DIR/dev"
	mount -R /run  "$DIR/run"
	mount -R /proc "$DIR/proc"
	# DNS
	cp /etc/resolv.conf "$DIR/etc/"
	# post installation script
	awk '{printf("%s\n",$0)}' "$TMP/post" >> "$CHT" 
	# unlock luks without initramfs
	printf '%s' "
CONFIG_CMDLINE_BOOL=y
CONFIG_CMDLINE=\"\\\"$OS,,,rw,$(dmsetup table --concise --showkeys $ROOT | awk -F ',' '{printf $NF}')\\\" root=/dev/dm-0\"
" >> "$TMP/.config"
	# portage config
	cp "$TMP/make.conf" "$DIR/etc/portage/"
	# disable signature for pacman
	ls "$TMP/pacman.conf" ||
	cp "$DIR/etc/pacman.conf" "$TMP/"
	awk '{gsub ("^[[:space:]]*SigLevel.*$", "SigLevel = Never", $0); gsub ("^[[:space:]]*CheckSpace.*$", "#CheckSpace", $0);}1' "$TMP/pacman.conf" >| "$DIR/etc/pacman.conf" 
	# execute chroot script
	printf "\n\n\n\n\n\n\n\n" | chroot "$DIR" sh /tmp/CHT
	# enable signature check for pacman
	awk '{gsub ("^[[:space:]]*SigLevel.*$", "SigLevel = Required DatabaseOptional", $0);}1' "$TMP/pacman.conf" >| "$DIR/etc/pacman.conf" 
	# link init if it's not
	ln -s "$(ls "$DIR/bin/" | awk '/-init/ {printf $1; exit}')" "$DIR/bin/init"
	# kernel version
	KVER=$(ls "$DIR/lib/modules/" | awk '{printf $1; exit}')
	# generate initramfs
	dracut -N -f -m " rootfs-block kernel-modules base crypt lvm " "$DIR/boot/cust.img" --kver "$KVER" -k "$DIR/lib/modules/$KVER"
	# grub config
	printf '%s' "
GRUB_DEFAULT=0
GRUB_TIMEOUT=2
GRUB_GFXMODE=auto
GRUB_TERMINAL=console
GRUB_TIMEOUT_STYLE=menu
GRUB_DISTRIBUTOR=\"$OS\"
GRUB_ENABLE_CRYPTODISK=y
GRUB_GFXPAYLOAD_LINUX=keep
GRUB_DISABLE_RECOVERY=true
GRUB_DISABLE_OS_PROBER=false
GRUB_EARLY_INITRD_LINUX_CUSTOM=\"cust.img\"
GRUB_PRELOAD_MODULES=\"lvm luks luks2 part_gpt cryptodisk gcry_rijndael pbkdf2 gcry_sha256 btrfs\"
GRUB_CMDLINE_LINUX_DEFAULT=\"root=UUID=$RUID resume=UUID=$RUID rd.luks.uuid=$LUID cryptdevice=UUID=$LUID:$OS cryptroot=UUID=$LUID cryptdm=$OS psi=1\"
	" >| "$DIR/etc/default/grub"
	# boot loader
	GIN="grub-install --boot-directory=. --efi-directory=. $DISK --target"
	printf '%s' "
cd /fat/
# legacy
$GIN=i386-pc
# efi
$GIN=x86_64-efi --removable
$GIN=x86_64-efi --bootloader-id=GRUB
grub-mkconfig >> grub/grub.cfg
# backup
cp grub/grub.cfg ./
	" >| "$TMP/grub"
	chroot "$DIR" sh /tmp/grub
	}
# installation begins
GO	() {

	## PROMPTS
	
 	# name this installation
	printf "
\033[2J\033[H 
 > NAME      : "
	read -r OS
	# choose distro
	printf "
[DISTRO ID]	| [DISTRO ID]
----------------+------------
 ARCH		|  VOID
 ARTIX		|  GENTOO
 FUNTOO		|  KISS
 ALPINE		|  NIXOS 

 > DISTRO ID : "
	read -r DID
	# list of packages to install
	printf "
 > packages  : "
	read -r PKG
	# select FAT device
	BLK | awk '/vfat/ {printf "\n\t"$1"\n"}'
	printf "
 > FAT  device : "
	read -r FAT
	# root device
	BLK | awk '/btrfs/ {printf "\n\t"$1"\n"}'
	printf "
 > ROOT device : "
	read -r ROOT
	
	## VARIABLES
	
	# / directory
	DIR="/tmp/$OS"
	# fat mount point
	FMP="$DIR/fat"
	# temporary directory
	TMP="$DIR/tmp"
	# path to save downloaded file
	DF="$TMP/DF"
	# chroot script
	CHT="$TMP/CHT"
	# memory
	MEM="0$(awk '/MemAvailable/ {printf $2}' /proc/meminfo)"
	# swap
	SWP="0$(awk 'NR==2 {printf $3}' /proc/swaps)"
 	# available swap + memory
 	ASM=$(printf '%s' "$MEM $SWP" | awk '{printf substr((($1+$2)/3000000)+1,1,1)}')
 	# threads
 	CPU=$(nproc)
 	# number of jobs
 	JOB=$(printf '%s' "$ASM $CPU" | awk '{printf ($1>=$2)*$2+($2>$1)*$1}')
	# disk
	DISK=$(BLK "$ROOT" | awk '/disk/ {printf $1; exit}')
	# LUKS uuid
	LUID=$(BLK "$ROOT" | awk '/LUKS/ {printf $4; exit}')
	# root uuid
	RUID=$(BLK "$ROOT" | awk 'NR==2 {printf $4}')
	# fat uuid
	FUID=$(BLK "$FAT" | awk 'NR==2 {printf $4}')
	# mirror
	MIR="https://mirrors.tuna.tsinghua.edu.cn"
	# architecture
	ARC=$(uname -m)
	# install kernel
	KIN="
cd /usr/src/linux-*/
cp /tmp/.config ./ || 
make localyesconfig
make
make INSTALL_MOD_STRIP=1 modules_install
make install
"
	export PATH="$PATH:/bin:/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/run/wrappers/bin:/root/.nix-profile/bin:/etc/profiles/per-user/root/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin" LOGNAME="$OS" MAKEOPTS="-j$JOB" MAKEFLAGS="-j$JOB"
	PRE
	"$DID" &&
	CHROOT &&
	printf '
\033[7m
run "passwd root" to set root password
Reboot, once done configuring
\033[m
' &&
	chroot "$DIR" sh
	}
ARCH	() {
	FET "$MIR/archlinux/iso/latest" ">archlinux"
	cp -r "$DIR/root.x86_64/." "$DIR/"
	awk '{printf substr($0,2)"\n"}' "$DIR/root.x86_64/etc/pacman.d/mirrorlist" >| "$DIR/etc/pacman.d/mirrorlist"
	rm -r "$DIR/root.x86_64"
	printf '%s' "
update-ca-trust
pacman-key --init
pacman-key --populate archlinux
pacman -Syu --overwrite '*' $PKG
	" >| "$CHT"
	}
VOID	() {
	FET "$MIR/voidlinux/live/current" ">void-$ARC-ROOTFS"
	printf '%s' "
rm -rf /var
xbps-install -R $MIR/voidlinux/current/ -S $PKG
xbps-remove base-voidstrap
xbps-remove -o
	" >| "$CHT"
	}
ARTIX	() {
	curl -OL codeberg.org/zz/strap/raw/branch/master/pacstrap.sh
	sh pacstrap.sh "$DIR"
	printf '%s' "
update-ca-trust
pacman -Syu --overwrite '*' $PKG
pacman-key --init
pacman-key --populate artix
	" >| "$CHT"
	}
GENTOO	() {
	FET "$MIR/gentoo/releases/$ARC/autobuilds/current-install-$ARC-minimal" ">stage3-$ARC-hardened-openrc" ||
	FET "$MIR/gentoo/releases/amd64/autobuilds/current-install-amd64-minimal" ">stage3-amd64-hardened-openrc"
	printf '%s' "
mkdir -p /etc/portage/repos.conf
cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
emerge-webrsync
env-update
source /etc/profile
USE=\"device-mapper\" emerge -uqDN --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going $PKG
$KIN
	" >| "$CHT"
	}
FUNTOO	() {
	GET build.funtoo.org/next/x86-64bit/generic_64/stage3-latest.tar.xz
	printf '%s' "
ego sync
USE=\"device-mapper\" emerge -uqDN --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going $PKG
$KIN
	" >| "$CHT"
	}
KISS	() {
	FET "github.com/kisslinux/repo/releases" "/download"
	FET "kernel.org" "/cdn.kernel.org/pub/linux" "longterm" "usr/src/"
	# kiss config
	printf '%s' "
export CFLAGS=\"-O3 -pipe -march=native\"
export CXXFLAGS=\"\$CFLAGS\"
export MAKEFLAGS=\"$MAKEFLAGS\"
export REPOS_DIR='/var/db/kiss'
export KISS_PATH=''
export KISS_SU=su
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/core
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/extra
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/wayland
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/community/community
	" >> "$DIR/etc/profile.d/kiss_path.sh"
	printf '%s' "
. /etc/profile.d/kiss_path.sh
cd \$REPOS_DIR/
git clone https://github.com/kisslinux/repo
git clone https://github.com/kiss-community/community
kiss b gnupg1
kiss i gnupg1
gpg --keyserver keys.gnupg.net --recv-key 13295DAC2CF13B5C
printf '\ntrusted-key 0x13295DAC2CF13B5C\n' >> /root/.gnupg/gpg.conf
cd repo/
git config merge.verifySignatures true
kiss update
kiss update
kiss b $PKG
kiss i $PKG
$KIN
	" >| "$CHT"
	}
ALPINE	() {
	FET "$MIR/alpine/latest-stable/releases/$ARC" ">alpine-minirootfs.*[^a-z].-$ARC" 
	printf '%s' "
apk add $PKG
	" >| "$CHT"
	}
NIXOS	() {
	useradd nixbld
	useradd bld -G nixbld
	mkdir -m 755 /nix "$FMP/grub" "$TMP/grub"
	curl -L nixos.org/nix/install | sh
	. "$HOME/.nix-profile/etc/profile.d/nix.sh"
	CHN=$(FLT NF "$MIR/nixos-images" ">nix")
	nix-channel --add "$MIR/nix-channels/$CHN" nixpkgs
	nix-channel --update
	nix-env -f '<nixpkgs>' -iA nixos-install-tools
	nixos-generate-config --root "$DIR"
	printf '%s' "
{ config, pkgs, ... }:
{
 imports = [ ./hardware-configuration.nix ];
 boot = {
	loader.grub = {
		version = 2;
		enable = true;
		device = \"$DISK\";
		efiSupport = true;
		useOSProber = true;
		enableCryptodisk = true;
		efiInstallAsRemovable = true;
		extraEntries = ''
			menuentry \"OTHER\" {
			search --set=root --fs-uuid $FUID
			configfile /grub/grub.cfg}'';
	};
	loader.efi = {
		efiSysMountPoint = \"/fat\";
	};
 };
 environment.systemPackages = [ $PKG ];
}
	" >| "$DIR/etc/nixos/configuration.nix"
	cp "$TMP/configuration.nix" "$DIR/etc/nixos/"
	printf "\n\n" | nixos-install --root "$DIR" &&
	printf '%s' "
menuentry \"$OS\" {
cryptomount -u $(printf '%s' "$LUID" | awk '{gsub("-","")}1')
search --set=root --fs-uuid $RUID
configfile /$OS/boot/grub/grub.cfg}
	" >> "$FMP/grub/grub.cfg"
	printf "
/nix/var/nix/profiles/system/activate
	" >| "$CHT"
	}

## CHECK DEPENDENCIES

type dracut awk curl btrfs tar xz gzip zstd cryptsetup && pvs && lsblk && GO || 
printf '
\033[7m
INSTALL dracut cryptsetup lvm2 btrfs-progs tar xz gzip zstd gawk curl util-linux
GRANT   root privileges
PROVIDE a valid DISTRO ID
\033[m
'
