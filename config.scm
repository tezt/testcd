(use-modules (gnu))
;(use-service-modules networking)
;(use-package-modules)
(operating-system
  (host-name "gix")
  (bootloader
    (bootloader-configuration
      (bootloader
        (bootloader
          (inherit grub-bootloader)
          (installer #~(const #t))))))
  (mapped-devices ; check
    (list 
       (mapped-device
         (source (uuid "" ""))
;        (source (list (uuid "" "")))
         (target "gix")
         (type luks-device-mapping))))
  (file-systems 
    (append
      (list
        (file-system
          (device (uuid "7f21a58f-01c1-45c6-9081-911b62624dcd"))
          (mount-point "/")
          (type "btrfs")
          (options "subvol=gix")
          (dependencies mapped-devices)) ;check
        (file-system
          (device (uuid "B3CE-491A" 'fat))
          (mount-point "/fat")
          (type "vfat")))
	  %base-file-systems))
; (packages 
;   (append 
;     (list 
;         )
;         %base-packages))
; (services
;   (append 
;     (list 
;          (service dhcp-client-service-type))
;          %base-services))
)
